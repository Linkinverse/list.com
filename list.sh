#!/bin/bash

while [ 1 ]
do
	# old version, show hidden files, and preview them
	#SELECTED=$(ls -a -I . | fzf --layout reverse --preview="cat {}" --preview-window=bottom:50%:wrap)
	# new version, previews files AND show directory tree
	SELECTED=$(ls -a -I . | fzf --layout reverse --preview="if [ -d {} ] ; then tree {} ; else cat {} ; fi" --preview-window=bottom:50%:wrap)	
	# strip size before filename in case you used ls -ash
	# i was unabe to strip the size for the cat preview in fzf
	#SELECTED=$(echo "$SELECTED" | awk -F ' ' '{print $2}')

	# user pressed CTRL-Q or file/directory does not exist
	if [ ! -e "$SELECTED" ]; then
		# detect if script is being sourced. it will exit to selected folder only if executed as: source script
		# usage example: alias list="source thisscript.sh".
		(return 0 2>/dev/null) && return || exit
	fi

	if [ -d "$SELECTED" ]; then
		# if directory change to that directory
		cd "$SELECTED"
	else
		# if file open it with default editor
		$EDITOR "$SELECTED"
	fi
done
