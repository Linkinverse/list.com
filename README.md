# list.com

A simple bash script for Linux, clone of Vernon D. Buerg's List.com file viewer for MS-DOS.

To easily navigate directories and open files, similar to [Ranger](https://github.com/ranger/ranger), or [nnn](https://github.com/jarun/nnn).

Since Buerg died in 2009 and his website went offline, a full backup of all his programs can be found here: https://github.com/sebras/buerg

## Installation
- `sudo cp list.sh /usr/bin`

- Add the following to your `~/.bashrc`: `alias list='source /usr/bin/list.sh'`

## Usage
Type `list` and use cursor keys to easily change directory, and ENTER to edit current selected file with your default editor. Press CTRL-Q to exit (to current directory).

## Similar projects
- [Linux List](https://github.com/Malvineous/linuxlist)

## Contributing
Feel fee to contribute forking or suggesting any improvement

## License
See LICENSE file.
